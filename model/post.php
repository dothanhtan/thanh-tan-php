<?php
class Post{
    var $id;
    var $title;
    var $content;
    var $description;

    function Post($id, $title, $content, $description)
    {
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
        $this->description = $description;    
    }
    static function connect(){
        $conn = new mysqli("localhost","root","","thanhtan");
        
        if($conn->connect_error)
            die("Kết nối thất bại. Chi tiết:" . $conn->connect_error);
        $conn->set_charset("utf8"); 
        return $conn;
    }
    static function getList(){
        //Buoc 1 Tao ket noi
        $con = Post::connect(); 
        //Buoc 2 Thao tac voi co so du lieu:CRUD
        $sql = "SELECT * FROM posts" ;
        $result = $con->query($sql);
        //var_dump($result);
        $listPost = array();
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
                $post = new Post($row["ID"], $row["Title"], $row["Content"], $row["Description"]);
                array_push($listPost, $post);
            }
        }
        //Buoc 3 Dong ket noi
        $con->close();
        return $listPost;
    }
    static function getPost($userID = null) {
        $con = Post::connect();
        $sql = "SELECT * FROM posts";
        if($userID != null) {
            $sql = "SELECT * FROM posts WHERE UserID = $userID";
        }
        $result = $con->query($sql);
        $arrPost = [];
        if($result->num_rows > 0) {
            while($row = $result->fetch_assoc()){
                $post = new Post($row["ID"], $row["Title"], $row["Content"], $row["Description"], $row["UserID"]);
                array_push($arrPost, $post);
            }
        }
        $con->close();
        return $arrPost;
    }

}
?>